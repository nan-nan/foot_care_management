import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
//登录注册改密码
const registor = resolve => require(['../page/login/registor.vue'], resolve)
const login1 = resolve => require(['../page/login/login1.vue'], resolve)
const login2 = resolve => require(['../page/login/login2.vue'], resolve)
const forgetPass = resolve => require(['../page/login/forgetPass.vue'], resolve)
const modifyPassword = resolve => require(['../frame/settingFrame/modifyPassword.vue'], resolve) //修改密码
// 首页
const index = resolve => require(['../page/index.vue'], resolve)
const home = resolve => require(['../page/home.vue'], resolve)
const device = resolve => require(['../page/device.vue'], resolve)
const order = resolve => require(['../page/order.vue'], resolve)
const my = resolve => require(['../page/my.vue'], resolve)
const myAccount = resolve => require(['../page/my/myAccount.vue'], resolve)
const NotFoundComponent = resolve => require(["../page/notFoundComponent.vue"], resolve) //404页面
export default new Router({
  //   mode: 'history',
  routes: [{
      path: '/',
      redirect: '/login2',
    },
    {
      path: '/index',
      component: index,
      'children': [{
          path: '/home',
          name: 'home',
          component: home,
        },
        {
            path: '/device',
            name: 'device',
            component: device,
          },
    
        {
          path: '/order',
          name: 'order',
          component: order,
        },
        {
            path: '/my',
            name: 'my',
            component: my,
          },
      ],
    },

    {
      path: '*',
      redirect: '/home',
    },
    {
      path: '/registor',
      name: 'registor',
      component: registor
    },
    {
        path: '/myAccount',
        name: 'myAccount',
        component: myAccount
      },
    {
      path: '/login1',
      name: 'login1',
      component: login1
    },
    {
      path: '/login2',
      name: 'login2',
      component: login2
    },
    {
      path: '/forgetPass',
      name: 'forgetPass',
      component: forgetPass
    },
    {
        path: '/NotFoundComponent',
        name: 'NotFoundComponent',
        component: NotFoundComponent
      },
  ],
})
