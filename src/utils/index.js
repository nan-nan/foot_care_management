import {Toast } from 'mint-ui';
function goodTime (str) {
  let now = new Date().getTime()
  let oldTime = new Date(str).getTime()
  let difference = now - oldTime
  let result = ''
  let minute = 1000 * 60
  let hour = minute * 60
  let day = hour * 24
  let month = day * 30
  let year = month * 12
  let _year = difference / year
  let _month = difference / month
  let _week = difference / (7 * day)
  let _day = difference / day
  let _hour = difference / hour
  let _min = difference / minute

  if (_year >= 1) {
    result = ' ' + ~~(_year) + ' 年前'
  } else if (_month >= 1) {
    result = ' ' + ~~(_month) + ' 个月前'
  } else if (_week >= 1) {
    result = ' ' + ~~(_week) + ' 周前'
  } else if (_day >= 1) {
    result =  ~~(_day) + ' 天前'
  } else if (_hour >= 1) {
    result = ' ' + ~~(_hour) + ' 个小时前'
  } else if (_min >= 1) {
    result = ' ' + ~~(_min) + ' 分钟前'
  } else {
    result = '刚刚'
  }
  return result
}
function _toast(msg){
  Toast({
    message: msg,
    position: 'bottom',
    duration: 2000
  });
}
//价格格式化
function priceFormat(value){
    var html,_val;
    value =Number(value).toFixed(2);
    if(!isNaN(value)) {
        if(value==0){
            value=0;
            return html = "<span>0</span>";
        }else if(value.split('.')[1].substring(1)==0){
            value = Number(value).toFixed(1);
        }
        _val = value.split('.');
        return html = '<span>'+_val[0]+'</span><em>.'+_val[1]+'</em>';
    }

}
//日期格式化 过滤器中用不了
function commentTime (date,fmt) {
    if(/(y+)/.test(fmt)){
        fmt = fmt.replace(RegExp.$1, (date.getFullYear()+'').substr(4-RegExp.$1.length));
    }
    let o = {
        "M+": date.getMonth() + 1, //月份
        "d+": date.getDate(), //日
        "h+": date.getHours(), //小时
        "m+": date.getMinutes(), //分
        "s+": date.getSeconds(), //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        "S": date.getMilliseconds() //毫秒
    }
    for(let k in o){
         if (new RegExp("(" + k + ")").test(fmt)) {
          fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
         }
    }
    return fmt;
}
//视频时间格式化
function vdoTime (s) {
    var t;
    if(s >= 0){
        var hour = Math.floor(s/3600);
        var min = Math.floor(s/60) % 60;
        var sec = s % 60;
        if(hour > 0) {
            if(hour < 10) {
                t = '0'+ hour + ":";
            } else {
                t = hour + ":";
            }
        } else {
            t = '';
        }
        if(min < 10){
            t += "0";
        }
        t += min + ":";
        if(sec < 10){
            t += "0";
        }
        t += Math.floor(sec);
    }
    return t;
    
}
// 计算两个日期间的时间差
function GetDateDiff(startTime, endTime, diffType) {  
    //将xxxx-xx-xx的时间格式，转换为 xxxx/xx/xx的格式  
    startTime = startTime.replace(/\-/g, "/");  
    endTime = endTime.replace(/\-/g, "/");  
    //将计算间隔类性字符转换为小写  
    diffType = diffType.toLowerCase();  
    var sTime = new Date(startTime);      //开始时间  
    var eTime = new Date(endTime);  //结束时间  
    //作为除数的数字  
    var divNum = 1;  
    switch (diffType) {  
        case "second":  
            divNum = 1000;  
            break;  
        case "minute":  
            divNum = 1000 * 60;  
            break;  
        case "hour":  
            divNum = 1000 * 3600;  
            break;  
        case "day":  
            divNum = 1000 * 3600 * 24;  
            break;  
        default:  
            break;  
    }  
    return parseInt((eTime.getTime() - sTime.getTime()) / parseInt(divNum));  
}  
// 获取json的长度
function getJsonLength(jsonobj) {
    var jsonLength = 0;  
    for(var item in jsonobj){  
        jsonLength++;  
    }  
    return jsonLength;  
}
//登录拦截
function isLogin(userid) {
    if(!userid) {
        this.$router.push({name:'login1'}) 
        _toast('请先登录!');
        return false;
    } 
}
//  键盘挡住输入框
function focus(e) {
    setTimeout(() =>{
        e.target.scrollIntoView(true);
    },200);
}
export default {
    goodTime,
    _toast,
    priceFormat,
    commentTime,
    vdoTime,
    GetDateDiff,
    getJsonLength,
    isLogin,
    focus,
}

