// 配置API接口地址
// var root = 'http://sxzd365.com'
// 引用axios
var axios = require('axios')

import MintUi from 'mint-ui';
import {
  Indicator,
  Toast
} from 'mint-ui';
// 自定义判断元素类型JS
function toType(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}
// 参数过滤函数
function filterNull(o) {
  for (var key in o) {
    if (o[key] === null) {
      delete o[key]
    }
    if (toType(o[key]) === 'string') {
      o[key] = o[key].trim()
    } else if (toType(o[key]) === 'object') {
      o[key] = filterNull(o[key])
    } else if (toType(o[key]) === 'array') {
      o[key] = filterNull(o[key])
    }
  }
  return o
}

function _toast(msg) {
  Toast({
    message: msg,
    position: 'bottom',
    duration: 2000
  });
}
//登录拦截
var that = this;

function isLogin(userid) {
  that.$router.push('/login1')
}

function apiAxios(method, root, url, params, success, failure, noLoad, credentials) {
  if (!noLoad) { //是否不显示load  默认否 即显示 即false  不显示(true)
    //   Indicator.open({
    //     //   text: '加载中...',
    //     //   spinnerType: 'fading-circle'
    //   });
  }
  if (params) {
    params = filterNull(params)
  }
  axios({
      method: method,
      url: url+'?token='+localStorage.getItem('token'),
    //   headers: {
    //     'x-access-token': localStorage.getItem('token')
    //   },
      data: method === 'POST' || method === 'PUT' ? params : null,
      params: method === 'GET' || method === 'DELETE' ? params : null,
      baseURL: root,
      withCredentials: credentials ? true : credentials //传的值是false 如果有 就显示false 否则true
    })
    .then(function (res) {
      if (res) {
        if (success) {
          success(res)
        }
      } else {
        if (failure) {
          failure(res)
        } else {

        }
      }
      Indicator.close();
    })
    .catch(function (err) {
      let res = err.response
      if (err) {
        _toast("网络不稳定,请稍后再试~");
        Indicator.close();
        return
      }
    })
}
// 返回在vue模板中的调用接口
export default {
  baseURLSchool: 'https://www.tiantong369.com', //客户服务器
  get: function (root, url, params, success, failure, noLoad) {
    return apiAxios('GET', root, url, params, success, failure, noLoad)
  },
  post: function (root, url, params, success, failure, noLoad, credentials) {
    return apiAxios('POST', root, url, params, success, failure, noLoad, credentials)
  },
  put: function (root, url, params, success, failure, noLoad) {
    return apiAxios('PUT', root, url, params, success, failure, noLoad)
  },
  delete: function (root, url, params, success, failure, noLoad) {
    return apiAxios('DELETE', root, url, params, success, failure, noLoad)
  },
}
