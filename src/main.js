

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
Vue.prototype.$ajax = axios
// 引用工具文件
import utils from './utils/index.js'
// 引用API文件
import api from './api/index.js'
//引入rem.js
import '../static/rem.js'
// 将API方法绑定到全局
Vue.prototype.$api = api
// 将工具方法绑定到全局
Vue.prototype.$utils = utils

// mint-ui
import { Toast,Indicator,Loadmore,MessageBox,Picker,DatetimePicker  } from 'mint-ui'
Vue.component(Loadmore.name, Loadmore)
Vue.component(DatetimePicker.name, DatetimePicker)
Vue.component(Picker.name, Picker)
//element-ui
// import {Pagination, Table,TableColumn,Button,Option, Form,
//     FormItem, Input,Col, DatePicker,TimePicker, Checkbox,CheckboxGroup, Radio,
//     RadioGroup,Switch,Rate, Upload,Select,Menu,Submenu,MenuItem,MenuItemGroup,}
//     from 'element-ui'
// Vue.use(Pagination)
// Vue.use(Table)
// Vue.use(TableColumn)
// Vue.use(Button)
// Vue.use(Option)
// Vue.use(Form)
// Vue.use(FormItem)
// Vue.use(Input)
// Vue.use(Col)
// Vue.use(DatePicker)
// Vue.use(TimePicker)
// Vue.use(Checkbox)
// Vue.use(CheckboxGroup)
// Vue.use(Radio)
// Vue.use(RadioGroup)
// Vue.use(Switch)

// Vue.use(Rate)
// Vue.use(Menu )
// Vue.use(MenuItem )
// Vue.use(Submenu )       
// Vue.use(MenuItemGroup )       
// Vue.use(Upload)
// Vue.use(Select)

// import BMap from 'BMap'

// 排序
import _ from 'lodash'

// 日历
// import 'vue-event-calendar/dist/style.css' //1.1.10之后的版本，css被放在了单独的文件中，方便替换
// import vueEventCalendar from 'vue-event-calendar'
// Vue.use(vueEventCalendar, {weekStartOn: 1, className: 'selected-day'}) //可以设置语言，支持中文和英文



Vue.config.productionTip = false

 
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
})

// 打包用到
document.addEventListener('deviceready', function() {
    new Vue({
      el: '#app',
      router,
      template: '<App/>',
      components: { App },
    })
    window.navigator.splashscreen.hide()
}, false);